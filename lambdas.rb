(lambda {puts "Hola mundo"}).call


miLambda1 = lambda {puts "Hola lambda"}

miLambda2 = ->() {puts "Hola lambda"}

miLambda1.call

miLambda2.call

miLambda3 = lambda  {|nombre| puts "Hola #{nombre}"}

miLambda4 = ->(nombre) {puts "Hola #{nombre}"}

miLambda3.call("Jerson")

miLambda4.call("Yaneth")


def test_block
	(Proc.new {return "Game Over Bloque"}).call
	puts "Despues de la bloque"
end


def test_lambda
	(->(){return "Game Over Bloque"}).call
	puts "Despues de la lambda"
end

puts test_lambda

puts test_block
