[1, 10].each { |number| puts number  }

impares = [1,2,3,4,5,6,7,8,9,10].select do |number|
	number % 2 != 0
end

puts impares


def llamabloque1
	yield
end

def llamabloque2 &bloque
	bloque.call if block_given?
end

llamabloque1 do
	puts "Hola bloque 1"
end

llamabloque2 do
	puts "Hola bloque 2"
end


class Usuario
	attr_accessor :nombre

	def saludar
		saludo = yield(@nombre)
		puts saludo
	end
end

jerson = Usuario.new

jerson.nombre = "Jerson"

jerson.saludar { |nombre| "Hello #{nombre}" }


def metodo proc1, proc2
	proc1.call
	proc2.call
end

p1 = Proc.new {puts "p1"}
p2 = Proc.new {puts "p2"}

metodo p1, p2