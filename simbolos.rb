
cadena = "Jerson"

cadena.capitalize!

puts cadena

#cadena inmutable, funciona igual que el String de Java
simbolo = :Jerson

#Por performance es mucho más alto trabajar con simbolos
s = simbolo.to_s + "Viveros"

puts s

