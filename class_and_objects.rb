#
class Video

	attr_accessor :minutes, :title

	#Método constructor
	def initialize(title)
		self.title=title
	end

	def play
	end

	def stop
	end

	def pause
	end
end

video1 = Video.new("V1")

video1.minutes = 120

video2 = Video.new("V2")

video2.minutes = 108

puts video1.title + " - "+video2.title