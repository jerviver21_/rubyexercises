print "Ingresa numero 1:"
numero_uno = gets.chomp.to_i
print "Ingresa numero 2:"
numero_dos = gets.chomp.to_i


#Condicionales
if numero_uno > numero_dos
	puts "#{numero_uno} es mayor que #{numero_dos}"
elsif numero_uno == numero_dos
	puts "Ambos numeros son iguales"
else
	puts "#{numero_uno} es menor que #{numero_dos}"
end

unless false
	puts "unless es un if invertido"
end

puts "#{numero_uno} es mayor que #{numero_dos}..." if numero_uno > numero_dos

puts "unless es un if invertido" unless false

#Operador ternario
puts (if numero_uno > numero_dos then "Mayor" else "Menor" end)

variable1 =  if numero_uno > numero_dos then "Mayor" else "Menor" end

puts variable1

variable2 = numero_uno > numero_dos ? "Mayor" : "Menor"

puts variable2

#case
print "Dame tu calificación:"
calificacion = gets.chomp.to_i

variable3 = case calificacion
				when 10, 9
					"Muy bien"
				when 8, 7
					"Puedes mejorar"
				when 6
					"Casi"
				else
					"u.u"
			end

puts variable3