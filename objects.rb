def square(x)
	x*x
end


puts square(2)

#splat operator (analogo a lista de argumentos)
def hola_gente(*personas)
	personas.each do |persona|
		puts "Hola #{persona}"
	end
end

hola_gente "Jerson", "Yaneth"

hola_gente *[3,2]

#keyword arguments

def hola(nombre: , edad:0, **options)
	puts nombre
	puts edad
	puts options
end

hola(edad:35, nombre:"Jerson", equipo:"America", ciudad:"Cali")