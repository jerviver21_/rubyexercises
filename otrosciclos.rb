#times
10.times do |i|
	puts i
end

5.times do 
	puts "no es necesario un indice"
end

#upto - También puede tener indice
1.upto(5) do
	puts "."
end

#downto
10.downto(5) do
	puts "X"
end