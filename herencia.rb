class Object
	def super_power
		puts "Todos los métodos tienen este metodo"
	end
end



class Video
	attr_accessor :title, :duracion, :descripcion

	private
	def embed_video
		"<video></video>"
	end
end

class YoutubeVideo < Video
	#variable de clase
	@@nombre_clase = "YoutubeVideo"
	attr_accessor :youtube_id

	def embed_video
		#super  #Si se quiere llamar al mismo metodo del padre
		"<iframe></iframe>"
	end

	#metodo de clase
	def self.nombre_clase
		@@nombre_clase
	end

end


yt = YoutubeVideo.new

yt.title = "Herencia Ruby"

p yt.title
