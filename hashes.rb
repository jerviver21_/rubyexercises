#Mapas
hash1 = {"nombre" => "Jerson", "apellido" => "Viveros", "edad" => 35}

puts hash1["nombre"]

hash1["apellido"] = "Viveros Aguirre"

puts hash1["apellido"]

hash1.default = "-"

#Usando las llaves con simbolos
hash2 = {nombre: "Jerson", apellido: "Viveros", edad: 35}

puts hash2

hash2.each do |k,v|
	puts "#{k}  -> #{v}"
end

#Operaciones con hashes
puts hash2.size

puts hash2.has_key?(:nombre)

puts hash2.has_value?("Jerson")

puts hash2.keys

puts hash2.values

hash2.delete(:edad)

puts hash2.key("Jerson")

hash3 = hash1.merge(hash2)

puts hash3

hash2.invert #cambia valores por claves y visceversa

hash2.clear

hash2.empty?